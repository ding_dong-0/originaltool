package com.tctel.originaltool;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OriginaltoolApplication {

    public static void main(String[] args) {
        SpringApplication.run(OriginaltoolApplication.class, args);
    }

}
