package com.tctel.originaltool.common.util;


import sun.misc.BASE64Encoder;

import java.io.*;
import java.util.Base64;

/**
 * @Description : Base64
 * @Author : zhuzl
 * @Date : 2018/04/25 11:15
 */
public class Base64Utils {

    private static final Base64.Decoder decoder = Base64.getDecoder();
    private static final Base64.Encoder encoder = Base64.getEncoder();

    /**
     * BASE64解密
     *
     * @param key
     * @return
     * @throws Exception
     */
    public static String decryptBASE64(String key) {
        return new String(decoder.decode(key));
    }

    /**
     * BASE64加密
     *
     * @param key
     * @return
     * @throws Exception
     */
    public static String encryptBASE64(String key) {
        byte[] bt = key.getBytes();
        return encoder.encodeToString(bt);
    }


    /**
     * 将 File 文件转化为 Base64 字符串
     *
     * @param file
     * @return
     */
    public static String getFileBase64(File file) {
        InputStream in = null;
        byte[] data = null;
        try {
            in = new FileInputStream(file);
            data = new byte[in.available()];
            in.read(data);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        //Base64编码
        BASE64Encoder encoder = new BASE64Encoder();
        return encoder.encode(data);
    }


    /**
     * 将 base64 字符串转化为 文件
     *
     * @param base64   base64的字符串
     * @param fileName 文件名字
     * @param filePath 存放的 xml 的文件夹
     */
    public static void base64ToFile(String base64, String fileName, String filePath) {
        File file = null;
        //创建文件目录 根据传入的 filePath 文件夹，如果不存在就创建
        File dir = new File(filePath);
        if (!dir.exists() && !dir.isDirectory()) {
            dir.mkdirs();
        }
        BufferedOutputStream bos = null;
        FileOutputStream fos = null;
        try {
            byte[] bytes = Base64.getMimeDecoder().decode(base64);
            file = new File(filePath + fileName);
            fos = new FileOutputStream(file);
            bos = new BufferedOutputStream(fos);
            bos.write(bytes);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (bos != null) {
                try {
                    bos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }


}
