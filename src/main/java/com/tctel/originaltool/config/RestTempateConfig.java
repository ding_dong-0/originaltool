package com.tctel.originaltool.config;


import com.tctel.originaltool.common.util.SSL;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.StandardCharsets;

@Configuration
public class RestTempateConfig {

    @Bean
    public RestTemplate restTemplate(ClientHttpRequestFactory ssl) {
        RestTemplate restTemplate = new RestTemplate(ssl);
        restTemplate.getMessageConverters().set(1, new StringHttpMessageConverter(StandardCharsets.UTF_8));//防止返回值乱码
        return restTemplate;
    }

    @Bean
    public ClientHttpRequestFactory simpleClientHttpRequestFactory() {
        SSL factory = new SSL();
        factory.setReadTimeout(3000000);
        factory.setConnectTimeout(3000000);
        return factory;
    }
}
