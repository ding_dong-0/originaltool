package com.tctel.originaltool.controller;

import com.tctel.originaltool.common.util.AES128Util;
import com.tctel.originaltool.common.util.Base64Utils;
import com.tctel.originaltool.common.util.FileUtils;
import com.tctel.originaltool.service.ForwardService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.io.File;

/**
 * 直接以 .xml 文件格式发送文件
 */
@Slf4j
@RestController
@EnableScheduling
public class TransportXMLController {

    //AES 加解密秘钥
//    String pwd = "huhaihdondl";

    @Value(value = "${AES.secret}")
    String pwd;

    //将要发送的 xml文件的存放目录
    @Value(value = "${original.xmlpath}")
    String xmlPath;
//    String xmlPath = "F:\\chuan\\xml";
//    String xmlPath = "F:\\chuan\\jieya\\yaotao\\xml";

    //解密后的xml文件存放目录
    @Value(value = "${getfromnei.filePath}")
    String filePath;  //从内网获取的数据所要报错的位置
//    String filePath = "F:\\chuan\\xml";


    @Resource
    ForwardService forwardService;


    /**
     * 直接以 .xml 文件格式发送文件
     * 因网络或者其他原因没有发送成功会进行重新发送；
     *
     * @return
     */
//    @Scheduled(initialDelay = 500, fixedDelay = 15000)
    public synchronized String secondXmlToServer() throws InterruptedException {
        log.info("===执行了从外网向内网发送XML数据===");

//        String xmlPath = "F:\\chuan\\xml";
        File file = new File(xmlPath);
        File[] files = readFolderFilterXMLFile(file);
        // 增强for形式  s遍历所有数组
        for (File f : files) {
            if (f.exists()) {
                long maxfilelenth = 25 * 1048576;//25M 的文件不传输
                //大文件将要移动到的文件名
                String tbg = "toobigfiles";
                //文件太大不传输
                if (f.length() > maxfilelenth) {
                    System.out.println("文件太大不传输");
                    String depath = xmlPath + tbg;
                    log.info("移动文件：从路径 " + f.getAbsolutePath() + " 移动到路径 " + depath);
                    //判断文件夹是否存在
                    File file1 = new File(depath);
                    FileUtils.judeDirExists(file1);
                    if (f.isFile()) {
                        File toFile = new File(depath + File.separator + f.getName());
                        if (toFile.exists()) {
                            log.info("文件已存在");
                        } else {
                            f.renameTo(toFile);
                            log.info("移动文件成功");
                        }
                    }
                } else {
                    String fileBase64 = Base64Utils.getFileBase64(f);
                    log.info("================================");
//                    log.info(fileBase64);
                    //===============================================================
                    // 将fileBase64 以 AES128Util进行加密
                    String password = AES128Util.pwdUtil(pwd);
                    //加密密文
                    String encrypt = AES128Util.encrypt(fileBase64, password);

                    String name = f.getName();
                    String nameAencrypt = name + "====>" + encrypt + "positive";
                    String s = forwardService.sendToGACS(nameAencrypt);

                    log.info("已成功发送文件{}", f);
                    //删除文件，发送成功后
                    //测试发送异常，是否删除
//            int i = 10 / 0;
                    /**
                     * 保存成功后返回的 信息，就可以删除已发送成功的文件
                     */
                    if (s.equals("filesavedsuccessfully")) {
                        System.out.println("返回发送成功");
                        boolean b = FileUtils.forceDelete(f);
                        if (b) {
                            log.info("已成功删除文件{}", f);
                            log.info("成功传送文件: {}", f);
                        } else {
                            log.info("文件{}删除失败", f);
                        }
//                log.warn("$$$$$$$$$$$$$====》成功传送文件{}《====$$$$$$$$$$$$$$$", f);
                    }
                }
            }




            /*
            // 解压到指定的目录

            // 将以AES128Util加密的 fileBase64 以 AES128Util进行解密
            String decrypt = AES128Util.decrypt(encrypt, password);

            System.out.println("=解密明文===>" + decrypt);


            //===============================================================
            // fileBase64 将要解密的 Base64 字符串
            //当前的时间戳.xml
            String fileName = System.currentTimeMillis() + ".xml";
            //String filePath = "F:\\chuan\\jieya\\yaotao\\xml";
            Base64Utils.base64ToFile(decrypt, fileName, filePath);

             */
            //让线程沉睡 1秒
//            Thread.sleep(1000);
        }
        return "";
    }


    /**
     * 只获取后缀为 .XML 的文件
     *
     * @param file
     * @return
     */
    private static File[] readFolderFilterXMLFile(File file) {
        File[] files = file.listFiles((dir, name) -> {
            //此处根据你需要的具体文件后缀
            if (name.endsWith(".XML") || name.endsWith(".xml")) {
                return true;
            }
            return false;
        });
        return files;
    }

    /**
     * 定时从内网获取指定文件夹下的xml文件 5000 毫秒= 5 秒
     */
    @Scheduled(initialDelay = 500, fixedDelay = 1000)
    public void GetFilesFromTheIntranet() {
        String s = "getfromnei";
        String s1 = forwardService.sendToGACS(s);
        if (s1.equals("failure")) {
            log.warn("边界异常");
        } else if (!s1.equals("satrt")) {
            System.out.println("================去删除的命令===================");
            log.info(s1);
            int i = s1.indexOf("====>");
            String xmlname = s1.substring(0, i);
            String xml = s1.replace(xmlname + "====>", "");
            log.info("需要保存的内网信息");
            log.info(xml);

            String s2 = saveFromNei(xml, xmlname);
            log.info(s2);
            // 发送删除文件的请求
            String ss = forwardService.sendToGACS(xmlname);
            log.warn("已保存的来自内网的xml文件：{}", xmlname);
            log.info("删除已保存的内网xml文件{}，返回结果{}", xmlname, ss);
        } else {
            log.warn("内网指定的文件夹没有xml文件");
        }
    }


    /**
     * 从内网获取到要传输的文件及其文件名解密并保存到本地指定目录
     *
     * @param encrypt
     * @param fileName
     * @return
     */
    public String saveFromNei(String encrypt, String fileName) {
        //  根据配置的pwd 通过 AES128Util生成密钥，用以解密被加密的字符串
        String password = AES128Util.pwdUtil(pwd);
        // 将以AES128Util加密的 fileBase64 文件 以 AES128Util进行解密
        String decrypt = AES128Util.decrypt(encrypt, password);

        log.info("===解密明文得到的文件明文===>" + decrypt);
        //===============================================================
        /**
         * decrypt  将要解密的 Base64 字符串
         * String filePath = "F:\\chuan\\jieya\\yaotao\\xml";
         * filePath 从内网获取文件的存放路径在配置文件中配置
         */
        Base64Utils.base64ToFile(decrypt, fileName, filePath);

        return "从内网获取的文件" + fileName + "保存成功";
    }


    Boolean flag = false;

    //    @Scheduled(initialDelay = 500, fixedDelay = 5000)
    public void task() throws InterruptedException {

        if (flag) {
            System.out.println();
            System.out.println();
            System.out.println();
            System.out.println();
            log.info("****************执行从内网获取文件至外网*************************");
            GetFilesFromTheIntranet();
            flag = false;
        } else {
            System.out.println();
            System.out.println();
            System.out.println();
            System.out.println();
            log.info("****************执行从外网推送文件到内网*************************");
            secondXmlToServer();
            flag = true;
        }
    }

}



