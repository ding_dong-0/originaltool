package com.tctel.originaltool.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;

@Service
@Slf4j
public class ForwardService {

//    String url = "http://localhost:8063/recevemessage";
    @Value(value = "${second.relayserver}")
    String url;

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    ForwardService forwardService;

    public String sendToGACS(String data) {
        HttpHeaders headers = new HttpHeaders();
        //headers 存放请求头信息 请求案例
        headers.add("Content-Type", "application/json;charset=utf-8");
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("data", data);
        HttpEntity httpEntity = new HttpEntity(hashMap, headers);
//        log.info("===发送的请求体==>{}", JSON.toJSONString(httpEntity));
        ResponseEntity<String> responseEntity = restTemplate.postForEntity(url, httpEntity, String.class);
        String body = responseEntity.getBody();
        log.info("===从内网返回的数据===>{}", body);
        return body;
    }


}
