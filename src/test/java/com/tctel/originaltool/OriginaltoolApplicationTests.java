package com.tctel.originaltool;

import com.tctel.originaltool.common.util.AES128Util;
import com.tctel.originaltool.common.util.Base64Utils;
import com.tctel.originaltool.common.util.FileUtils;
import com.tctel.originaltool.service.ForwardService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;

@Slf4j
@SpringBootTest
class OriginaltoolApplicationTests {

    /**
     * AES128Util  测试案例
     */
    @Test
    void contextLoads() {
        String s = "kikjoiwmo9908";
        String pwd = AES128Util.pwdUtil(s);
        System.out.println(pwd);
        String ms = "西安市未央区西安国际花园";
        System.out.println("====================要传输的信息====================");
        System.out.println("======>" + ms);
        String encrypt = AES128Util.encrypt(ms, pwd);
        System.out.println("====================加密后的字符串====================");
        System.out.println(encrypt);
        String decrypt = AES128Util.decrypt(encrypt, pwd);
        System.out.println("====================解密后的字符串====================");
        System.out.println(decrypt);

    }


    @Autowired
    ForwardService forwardService;

    @Test
    void co() {
        String s = "038CE623872C2C9EF1FEBB44CAE9CE4BF21DF6F955E40B7C786B6384A4DFF8CEBAFB169926956CACE5BB89815753E34BD302DB7DD847FA9ABF1352B79B577C8703B725C4C836C29D77014742087047CCD90433A4A58C6B157AE2563346340EBAA588002FF7970781984404D6854396022224EA30CBA3C61306E3B55DF5B176CEA3FA2E32F9389096836DA2A762674DD262C8AA54898D2DA36AC1063EF817D2A3BAE941C5A282BE0C12EB5CC82D6D0F9AFE7DCFB20DB2C5937139F049ADA6B23817B4480C5B4C9A9821BF23F679B116267ECA5EE7D9C17C55211FF3E2D4AD43422F0772EDB6A01618B6ADDF3213C0A75EC50842079473C38B6D66C97DBBE3D069B72C21E5FB5109A16A09B22E60FB70BAD8ED8388BB89F42562177EACBD45795E046466CA94CD759F92EA26F1B0FF797DB3011A87782A07F8E84160C8E925FE3C7FCA27242F1BA5C1644A8C27ADA8A2FEC2A2FF5674749EF481FB2DDD32647E81A2203FCAB678F794517A687D318E4648F4D9B5BE1836D91EBF9DFEA74FF5DD62E4EE0AD3DBB24C44C320CC4B9A0758720FF6BBFDC8C48603279304650E35410AC0548CDC17A86F2018F9D41D64055D54106674757FE8160E39C1D1A4A96288D99941B0C2F67907F930C88FCF6FA1888F6E27E25C0FA46F70716253D711A26E6FED80E7B67C2F04AEDA0772DC0E853B1EEFE82790FD4115152B345C71003EADB29C0EFC1DA6C043B0B7A85710AF5516E35C36B9F511EC02031A4579CCF830D75CEE0DB600957322B9492C666950669EED9A64B3BF9FD3D18B058D1504CE641C65EE7ED38EA61B56F1503D02D7651CFBF027DCDDED0459D26A94DD32FAE1FFB311";
        String s1 = forwardService.sendToGACS(s);
        System.out.println(s1);
    }

    @Test
    void testdelete() {
        String s = "C:\\Users\\Administrator\\Desktop\\数据传输\\边界项目代码\\secondtool\\logs\\secondtool\\log_info.log";
        File file = new File(s);
        boolean b = FileUtils.forceDelete(file);
        System.out.println(b);
        file.exists();
    }

    @Test
    void testfilenull() {
        File f = null;
        f.exists();
        System.out.println("========");
        if (f.exists() && f != null) {
            Base64Utils.getFileBase64(f);
        }
    }

    @Test
    void test0() {
//        long length = f.length();
//        String size = FileUtils.FormetFileSize(length);
//        long zddx="25 M";
        String filepath = "E:\\天创工作\\已完成\\数据传输\\边界项目代码 - 副本\\originaltoolpositive.jar";
        File file = new File(filepath);
        long maxfilelenth = 20 * 1048576;
        if (file.length() < maxfilelenth) {
            System.out.println("文件小于");
        } else {
            System.out.println("文件大大");
        }


    }


    @Test
    void test1() {

        String fromPath = "i:\\test\\hello.txt";
        String toPath = "i:\\test\\";
        File f = new File(fromPath);


        long maxfilelenth = 10 * 1048576;
        //大文件将要移动到的文件名
        String tbg = "toobigfiles";
        if (f.length() > maxfilelenth) {
            System.out.println("文件太大不传输");
            String depath = toPath + tbg;
            log.info("移动文件：从路径 " + f.getAbsolutePath() + " 移动到路径 " + depath);
            //判断文件夹是否存在
            File file1 = new File(depath);
            FileUtils.judeDirExists(file1);
            if (f.isFile()) {
                File toFile = new File(depath + File.separator + f.getName());
                if (toFile.exists()) {
                    log.info("文件已存在");
                } else {
                    f.renameTo(toFile);
                    log.info("移动文件成功");
                }
            }
        }
    }


    @Test
    void test2() {
        System.out.println(File.separator);
    }

    @Test
    void test3() {
    }

    @Test
    void test4() {
    }

    @Test
    void test5() {
    }

    @Test
    void test6() {
    }

    @Test
    void test7() {
    }

    @Test
    void test8() {
    }

    @Test
    void test9() {
    }


}
